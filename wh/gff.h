/*  File: gff.h
 *  Author: Ed Griffiths (edgrif@sanger.ac.uk)
 *  Copyright (c) J Thierry-Mieg and R Durbin, 2003
 *-------------------------------------------------------------------
 * Acedb is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, written by
 * 	Richard Durbin (Sanger Centre, UK) rd@sanger.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.crbm.cnrs-mop.fr
 *
 * Description: Interface to routines for dumping segs in gff-like
 *              formats. The main routine, GFFDump(), interrogates the
 *              segs and extracts information from them, but then
 *              calls the supplied GffCallBack routine which will then
 *              do the output.
 *              The intention is to have just _one_ routine that loops
 *              through the segs even though we may want to output the
 *              information as GFF, DASGFF or whatever.
 * HISTORY:
 * Last edited: Aug 10 07:18 2005 (edgrif)
 * Created: Thu Jan 30 14:42:21 2003 (edgrif)
 * CVS info:   $Id: gff.h,v 1.13 2005/08/10 08:13:03 edgrif Exp $
 *-------------------------------------------------------------------
 */
#ifndef DEF_GFF_H
#define DEF_GFF_H

#define GFF_UNKNOWN_FIELD "."


typedef struct GFFLineStruct_
{
  KEY   seqKey;
  char *methodName;
  BOOL is_structural;
  char *sourceName;
  char *featName;
  int   x, y;
  float score;
  BOOL  isScore;
  char  strand;
  char  frame;
  char *attributes;
  char *comment;
} GFFLineStruct, *GFFLine;



/* GFFDump can output a list of features or keys instead of the usual GFF output. */
typedef enum {GFF_LIST_NONE, GFF_LIST_FEATURES, GFF_LIST_KEYS} GffListType ;


/* Callbacks for GFF dumping, caller must pass a function that will take the processed
 * GFF record and output it in a suitable style. app_data may be used to pass information
 * through to this callback. */
typedef void (*GffCallBack)(void *app_data,
			    char *seqname, char *seqclass, char *method,
			    BOOL mapped,
			    char *source, char *feature, int start, int end,
			    float *score, char strand, char frame,
			    char *attribute, char *comment, int segType) ;

/* Get span of features etc. ready for input into GFFDump. */
BOOL GFFRefSeqPos(FeatureMap look, int version,
		  KEY *refSeq, KEY *seqKey_out, BOOL *reversed_out,
		  int *offset_out,
		  int *key_start_out, int *key_end_out,
		  int *feature_start_out, int *feature_end_out) ;

/* Output the data, note that app_cb must be supplied. */
BOOL GFFDump(GffCallBack app_cb, void *app_data,
	     FeatureMap look, int version, 
	     KEY seqKey, int offset, int feature_start, int feature_end,
	     GffListType list_type, DICT listset, BOOL raw_methods, Array stats,
	     DICT *sourceSet, DICT *featSet, DICT *methodSet,
	     BOOL include_source, BOOL include_feature, BOOL include_method,
	     BOOL zmap_dump) ;

#endif /*  !defined DEF_GFF_H */
