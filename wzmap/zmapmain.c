/*  Last edited: Oct 31 11:26 2003 (rnc) */



int main (int argc, char *argv)
{
  KEY key;
  KEY from;
  BOOL isOldGraph;
  void *app_data;

  if (argc != 4)
    {
      printf("Wrong number of args: %d; should be 4\n", argc);
      return FALSE;
    }
  key  = atoi(argv[2]);
  from = atoi(argv[3]);
  isOldGraph = atoi(argv[4]);
  app_data = argv[5];

  return zMapDisplay(key, from, isOldGraph, app_data);
}
