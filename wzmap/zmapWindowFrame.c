/*  File: zmapappconnect.c
 *  Author: Ed Griffiths (edgrif@sanger.ac.uk)
 *  Copyright (c) Sanger Institute, 2003
 *-------------------------------------------------------------------
 * ZMap is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *-------------------------------------------------------------------
 * This file is part of the ZMap genome database package
 * and was written by
 *      Rob Clack (Sanger Institute, UK) rnc@sanger.ac.uk,
 * 	Ed Griffiths (Sanger Institute, UK) edgrif@sanger.ac.uk and
 *	Simon Kelley (Sanger Institute, UK) srk@sanger.ac.uk
 *
 * Description: 
 * Exported functions: See XXXXXXXXXXXXX.h
 * HISTORY:
 * Last edited: Nov 14 14:33 2003 (edgrif)
 * Created: Thu Jul 24 14:36:37 2003 (edgrif)
 * CVS info:   $Id: zmapWindowFrame.c,v 1.1 2003/11/20 14:37:30 rnc Exp $
 *-------------------------------------------------------------------
 */

#include <stdlib.h>
#include <stdio.h>
#include <zmapWindow_P.h>


GtkWidget *zmapWindowMakeFrame(ZMapWindow window)
{
  GtkWidget *frame ;
  GtkWidget *vbox, *text, *drawing_area ;
  gchar *frame_title ;

  frame_title = g_strdup_printf("Sequence : %s", window->sequence) ;

  frame = gtk_frame_new(frame_title);
  gtk_frame_set_label_align(GTK_FRAME(frame), 0.0, 0.0 );
  gtk_container_border_width(GTK_CONTAINER(frame), 5);

  g_free(frame_title) ;

  vbox = gtk_vbox_new(FALSE, 0) ;
  gtk_container_border_width(GTK_CONTAINER(vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  /* CODE TO CREATE THE ACTUAL ZMAP SHOULD GO HERE INSTEAD OF THIS DUMMY DATA.......... */
  
  window->text = text = gtk_text_view_new() ;
  gtk_widget_set_usize(text, 500, 500) ;
  gtk_container_add(GTK_CONTAINER(vbox), text);


  return frame ;
}

