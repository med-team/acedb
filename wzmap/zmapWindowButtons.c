/*  File: zmapWindow.c
 *  Author: Ed Griffiths (edgrif@sanger.ac.uk)
 *  Copyright (c) Sanger Institute, 2003
 *-------------------------------------------------------------------
 * ZMap is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *-------------------------------------------------------------------
 * This file is part of the ZMap genome database package
 * and was written by
 *      Rob Clack (Sanger Institute, UK) rnc@sanger.ac.uk,
 * 	Ed Griffiths (Sanger Institute, UK) edgrif@sanger.ac.uk and
 *	Simon Kelley (Sanger Institute, UK) srk@sanger.ac.uk
 *
 * Description: 
 * Exported functions: See XXXXXXXXXXXXX.h
 * HISTORY:
 * Last edited: Nov 18 10:55 2003 (edgrif)
 * Created: Thu Jul 24 14:36:27 2003 (edgrif)
 * CVS info:   $Id: zmapWindowButtons.c,v 1.1 2003/11/20 14:37:28 rnc Exp $
 *-------------------------------------------------------------------
 */

#include <string.h>
#include <zmapWindow_P.h>

static void loadCB(GtkWidget *widget, gpointer cb_data) ;
static void stopCB(GtkWidget *widget, gpointer cb_data) ;
static void quitCB(GtkWidget *widget, gpointer cb_data) ;


GtkWidget *zmapWindowMakeButtons(ZMapWindow window)
{
  GtkWidget *frame ;
  GtkWidget *hbox,  *load_button, *stop_button, *quit_button ;

  frame = gtk_frame_new("Actions");
  gtk_frame_set_label_align(GTK_FRAME( frame ), 0.0, 0.0 );
  gtk_container_border_width(GTK_CONTAINER(frame), 5);

  hbox = gtk_hbox_new(FALSE, 0) ;
  gtk_container_border_width(GTK_CONTAINER(hbox), 5);
  gtk_container_add(GTK_CONTAINER(frame), hbox);

  load_button = gtk_button_new_with_label("Load Data") ;
  gtk_signal_connect(GTK_OBJECT(load_button), "clicked",
		     GTK_SIGNAL_FUNC(loadCB), (gpointer)window) ;
  gtk_box_pack_start(GTK_BOX(hbox), load_button, FALSE, FALSE, 0) ;

  stop_button = gtk_button_new_with_label("Stop") ;
  gtk_signal_connect(GTK_OBJECT(stop_button), "clicked",
		     GTK_SIGNAL_FUNC(stopCB), (gpointer)window) ;
  gtk_box_pack_start(GTK_BOX(hbox), stop_button, FALSE, FALSE, 0) ;

  quit_button = gtk_button_new_with_label("Quit") ;
  gtk_signal_connect(GTK_OBJECT(quit_button), "clicked",
		     GTK_SIGNAL_FUNC(quitCB), (gpointer)window) ;
  gtk_box_pack_start(GTK_BOX(hbox), quit_button, FALSE, FALSE, 0) ;

  /* default button is quit...probably not the most sensible.... */
  GTK_WIDGET_SET_FLAGS(quit_button, GTK_CAN_DEFAULT) ;
  gtk_window_set_default(GTK_WINDOW(window->toplevel), quit_button) ;


  return frame ;
}




/*
 *  ------------------- Internal functions -------------------
 */


/* We should record the button widget and have one routine that acts according to the
 * button widget. */

static void loadCB(GtkWidget *widget, gpointer cb_data)
{
  ZMapWindow window = (ZMapWindow)cb_data ;

  (*(window->app_routine))(window->app_data, ZMAP_WINDOW_LOAD) ;

  return ;
}

static void stopCB(GtkWidget *widget, gpointer cb_data)
{
  ZMapWindow window = (ZMapWindow)cb_data ;

  (*(window->app_routine))(window->app_data, ZMAP_WINDOW_STOP) ;

  return ;
}


static void quitCB(GtkWidget *widget, gpointer cb_data)
{
  ZMapWindow window = (ZMapWindow)cb_data ;

  (*(window->app_routine))(window->app_data, ZMAP_WINDOW_QUIT) ;

  return ;
}

