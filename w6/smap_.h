/*  File: smap_.h
 *  Author: Ed Griffiths (edgrif@sanger.ac.uk)
 *  Copyright (c) Sanger Institute, 2003
 *-------------------------------------------------------------------
 * ZMap is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *-------------------------------------------------------------------
 * This file is part of the ACEDB genome database package, originated by
 * 	Richard Durbin (Sanger Centre, UK) rd@sanger.ac.uk, and
 *	Jean Thierry-Mieg (CRBM du CNRS, France) mieg@kaa.crbm.cnrs-mop.fr
 *
 * Description: Data structs etc. internal to SMap.
 * HISTORY:
 * Last edited: May  6 14:38 2005 (edgrif)
 * Created: Fri Oct 31 09:45:55 2003 (edgrif)
 * CVS info:   $Id: smap_.h,v 1.3 2005/05/12 11:17:10 edgrif Exp $
 *-------------------------------------------------------------------
 */
#ifndef DEF_SMAP_P_H
#define DEF_SMAP_P_H

#include <wh/smap.h>					    /* External header. */



/********** data structures ****************************************/

typedef struct
{
  int s1, s2 ;	/* start/end of segments allowed to disagree, 
		   in source coordinates */
} SMapMismatch ;



#define invertStrand(x) ((x) == STRAND_DOWN ? STRAND_UP : STRAND_DOWN)



/* For each key, the keyInfo record gives an array for the map and an
   array of mismatch information, which can be null.

   It also records:

   - the strand of this key relative to its parent, this is needed because otherwise we cannot
   tell the strand of features that are 1 base long.

   - The length of this key in its parent is also recorded because the coordinates
   in the "map" array may have been clipped and hence cannot reliably be used
   for positioning of child features that are reversed with respect to this
   keys coordinates (i.e. we always know the key starts at "1" and hence clipping
   of the start does not matter, but if the end is clipped we have no way of knowing
   the length unless we record it separately.

   - In a similar way we also need to know how we were aligned into our parent,
   we may have been specified with an  S_Child.........Align parent_start child_start.
   We need this child_start information so we can give sMapLocalMap() all the information it needs
   to work.

   Each SMapMap structure in map defines a set of intervals in the 
   coordinate system of key, each interval being mapped as an ungapped
   block to a corresponding interval in the SMap's coordinates.
   
   Each [s1,s2] pair in mismatch defines a set of intervals in the 
   coordinate system of key that can contain mismatches to the canonical 
   sequence.

   Both ->map and ->mismatch arrays are sorted in increasing order of s1, 
   and s2 >= s1 always.  We also assume here that the sequences are 
   colinear.
*/
struct SMapKeyInfoStruct
{
  KEY key, parent ;
  Strand strand ;					    /* vital for features of length = 1 */
  int start_from_parent;				    /* Our offset in our parent. */
  int length ;						    /* Of this object in the parent. */
  SMap *smap ;
  Array map ;						    /* of SMapMap */
  Array inverseMap ;					    /* of SMapMap - filled lazily */
  Array mismatch ;					    /* of SMapMisMatch */
  Array children ;					    /* of SMapKeyInfo* */
} ;



/* This is the main structure representing an SMap, this points to all the other information
 * for an SMap. */
struct SMapStruct
{
  STORE_HANDLE handle ;		/* the SMap's own handle */
  Associator key2index ;	/* index into keyInfo */
  SMapKeyInfo *root ;
  int length ;
  int area1, area2;

  /* following is a simple cache for key2index mapping */
  SMapKeyInfo *lastKeyInfo ;

#ifdef ACEDB4
  Array lastKeyUnspliced;
#endif
} ;


/* Struct used to return result of mapping, y1 & y2 are the original coordinates remapped into
 * root coordinates. nx1 & nx2 are the original coordinates truncated to fit into the root
 * coordinate system, if no truncation was necessary then they will be original coordinates. */
struct SMapMapReturn
{
  int y1, y2;						    /* root co-ordinates */
  int nx1, nx2;						    /* truncated local co-ordinates */
};




/* Used by internal exon sorting routines. */
typedef struct { int x, y ; } ExonStruct ;




#endif /* DEF_SMAP_P_H */
